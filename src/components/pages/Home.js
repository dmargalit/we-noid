import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withAuth } from '@okta/okta-react';
import { Button } from 'antd';

export default withAuth(
  class Home extends Component {
    state = { authenticated: null };

    checkAuthentication = async () => {
      const authenticated = await this.props.auth.isAuthenticated();
      if (authenticated !== this.state.authenticated) {
        this.setState({ authenticated });
      }
    };

    async componentDidMount() {
      this.checkAuthentication();
    }

    async componentDidUpdate() {
      this.checkAuthentication();
    }

    login = async () => {
      this.props.auth.login('/');
    };

    logout = async () => {
      this.props.auth.logout('/');
    };

    render() {
      if (this.state.authenticated === null) return null;

      const mainContent = this.state.authenticated ? (
        <div>
          <p className="lead">
            You have entered the product portal,{' '}
            <Link to="/product">click here</Link>
          </p>
          <Button type="primary" onClick={this.logout}>
            Logout
          </Button>
        </div>
      ) : (
        <div>
          <p className="lead">
            If you are a product member, please get your credentials from your
            supervisor
          </p>
          <Button type="primary" className="btn btn-dark btn-lg" onClick={this.login}>
            Login
          </Button>
        </div>
      );

      return (
        <div className="jumbotron">
          <h1 className="display-4">WeNoid MVP</h1>
          {mainContent}
        </div>
      );
    }
  }
);
